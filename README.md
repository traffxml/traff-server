**traff-server** is a server which aggregates traffic information from various sources and makes them available to apps (“consumers”) in TraFF format.

It is written in 100% pure Java. Although mainly developed to run on Linux, it should be portable enough to run on any platform which supports Java.

# Components

traff-server is modular and allows for distributed installation across multiple systems.

* **SubscriptionManager** is the HTTP server front-end to which clients connect.
* **PushReceiver** (not yet implemented) is an HTTP server front-end which receives push updates from sources which support this.
* **MessageCache** is the data storage library.
* **TraffReceiver** is a daemon which polls preconfigured sources for new messages, and handles subscriptions to push sources.

Other directories in the code tree:

* **debian**: Packaging information for Debian-based Linux distributions
* **doc**: Documentation

## SubscriptionManager

This is the client HTTP front-end. Clients can connect to it, subscribe to a particular set of messages and periodically poll it. If new messages have arrived, the subscription manager will deliver to the client all messages which have changed since the last poll.

## PushReceiver

Some sources send updates by pushing them to a web server. This update mechanism is handled by the PushReceiver, which receives the feeds, converts them to TraFF and updates the message cache. This functionality depends on additional TraFF converter libraries.

## MessageCache

The message cache is the central data store of the TraFF server. It holds all currently active messages, as well as all active subscriptions.

The **MessageCache** folder in this repository contains a library through which the other components access the message cache. The data store itself is a database, which is accessed over JDBC. This has been tested with HSQLDB 2.1.4 running in server mode. Other DBMSes may work but are untested.

## TraffReceiver

The TraFF receiver is a daemon which does two things:

* For pull-only sources, it requests an update from the source at a configurable interval, converts the data into TraFF and updates the message cache. This functionality depends on additional TraFF converter libraries.
* For push sources, it registers the push receiver instance with the source.

# Known issues

MessageCache currently fails to stop cleanly on Ubuntu 20.04. This is presumably caused by a bug (https://bugs.launchpad.net/ubuntu/+source/hsqldb/+bug/1909811) in Ubuntu 20.04. Other components are not affected and can be installed on top of Ubuntu 20.04 in a distributed environment.
