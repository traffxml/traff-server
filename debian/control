Source: traff-server
Section: net
Priority: optional
Maintainer: Michael von Glasow <michael@vonglasow.com>
Build-Depends: debhelper (>= 12), openjdk-8-jre-headless | openjdk-11-jre-headless, maven
Standards-Version: 4.3.0
Homepage: http://traffxml.org
Vcs-Git: https://gitlab.com/traffxml/traff-server

Package: traff-server-receiver
Architecture: all
Pre-Depends: openjdk-8-jre-headless | openjdk-11-jre-headless
Depends: ${misc:Depends}
Description: TraFF traffic information server (receiver component)
 TraFF Server is a service which gathers traffic information from a set of
 configurable sources and makes them available over an HTTP API in a
 standardized format.
 .
 This package contains the TraFF Receiver, which polls sources, converts
 information to TraFF and stores them in the cache database.

Package: traff-server-message-cache
Architecture: all
Pre-Depends: openjdk-8-jre-headless | openjdk-11-jre-headless
Depends: ${misc:Depends}, libhsqldb-java, libslf4j-java
Description: TraFF traffic information server (message cache component)
 TraFF Server is a service which gathers traffic information from a set of
 configurable sources and makes them available over an HTTP API in a
 standardized format.
 .
 This package contains the message cache, which acts as the storage backend for
 messages and subscriptions.
