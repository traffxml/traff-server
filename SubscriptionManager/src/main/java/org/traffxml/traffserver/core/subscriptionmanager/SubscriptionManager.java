/*
 * Copyright © 2017–2020 Michael von Glasow.
 * 
 * This file is part of TraFF Server.
 *
 * TraFF Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TraFF Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TraFF Server.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traffserver.core.subscriptionmanager;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.traff.TraffFeed;
import org.traffxml.traff.Version;
import org.traffxml.traff.subscription.Capabilities;
import org.traffxml.traff.subscription.Subscription;
import org.traffxml.traff.subscription.TraffRequest;
import org.traffxml.traff.subscription.TraffResponse;
import org.traffxml.traff.subscription.TraffResponse.Status;
import org.traffxml.traffserver.core.messagecache.MessageCache;

/**
 * Servlet implementation class SubscriptionManager
 */
@WebServlet(description = "TraFF Subscription Manager", urlPatterns = { "/*" })
public class SubscriptionManager extends HttpServlet {
	/**
	 * @brief The logger for log output.
	 * 
	 * Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private static final long serialVersionUID = 1L;

	/** Milliseconds per minute */
	private static final int MILLIS_PER_MINUTE = 60000;

	/**
	 * Separator character for property keys.
	 */
	private static final String PROP_KEY_SEPARATOR = ".";

	/**
	 * Prefix for properties parsed by the subscription manager.
	 */
	private static final String PROP_KEY_SUBSCRIPTION_PREFIX = "subscription" + PROP_KEY_SEPARATOR;

	/**
	 * Property key for the subscription timeout.
	 * 
	 * If left empty, the default value of {@link #subscriptionTimeout} will be used.
	 */
	private static final String PROP_KEY_TIMEOUT = PROP_KEY_SUBSCRIPTION_PREFIX + "timeout";

	private MessageCache cache;

	private Properties properties = new Properties();

	/** The timer to purge expired subscriptions from the cache */
	private static Timer subscriptionExpirationTimer = new Timer("subscriptionExpirationTimer", true);

	/**
	 * Timeout for subscriptions, in seconds.
	 * 
	 * This is read from properties on startup; if missing or invalid, the default of 3600 s (1 hour) is used.
	 */
	private int subscriptionTimeout = 3600;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SubscriptionManager() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		TraffResponse res;
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType("application/xml");
		try {
			TraffRequest traffReq = TraffRequest.read(request.getInputStream());
			Subscription subscription;
			TraffFeed feed;
			switch(traffReq.operation) {
			case GET_CAPABILITIES:
				res = new TraffResponse(Status.OK, null, null, new Capabilities(Version.V0_8, Version.V0_8), null);
				break;
			case SUBSCRIBE:
				subscription = cache.subscribe(traffReq.getFilterListAsList(), subscriptionTimeout);
				feed = cache.poll(subscription.id);
				res = new TraffResponse(Status.OK, subscription.id, subscriptionTimeout, null, feed);
				break;
			case CHANGE:
				try {
					subscription = cache.changeSubscription(traffReq.subscriptionId,
							traffReq.getFilterListAsList());
					feed = cache.poll(traffReq.subscriptionId);
					res = new TraffResponse(Status.OK, null, null, null, feed);
				} catch (IllegalArgumentException e) {
					/* subscription does not exist, quote subscription ID in the error message */
					res = new TraffResponse(Status.SUBSCRIPTION_UNKNOWN, traffReq.subscriptionId, null, null, null);
				}
				break;
			case UNSUBSCRIBE:
				cache.unsubscribe(traffReq.subscriptionId);
				res = new TraffResponse(Status.OK, null, null, null, null);
				break;
			case POLL:
				try {
					feed = cache.poll(traffReq.subscriptionId);
					res = new TraffResponse(Status.OK, null, null, null, feed);
				} catch (IllegalArgumentException e) {
					/* subscription does not exist, quote subscription ID in the error message */
					res = new TraffResponse(Status.SUBSCRIPTION_UNKNOWN, traffReq.subscriptionId, null, null, null);
				}
				break;
			case HEARTBEAT:
				try {
					cache.touchSubscription(traffReq.subscriptionId);
					res = new TraffResponse(Status.OK, null, null, null, null);
				} catch (IllegalArgumentException e) {
					/* subscription does not exist, quote subscription ID in the error message */
					res = new TraffResponse(Status.SUBSCRIPTION_UNKNOWN, traffReq.subscriptionId, null, null, null);
				}
				break;
			default:
				/* anything else is an invalid request, quote subscription ID if known */
				res = new TraffResponse(Status.INVALID, traffReq.subscriptionId, null, null, null);
			}
		} catch (Exception e) {
			/* if the request could not be parsed, treat it as invalid */
			res = new TraffResponse(Status.INVALID, null, null, null, null);
		}
		try {
			res.write(response.getOutputStream());
		} catch (Exception e) {
			LOG.debug("{}", e);
			throw new ServletException(e);
		}
	}

	@Override
	public void init() throws ServletException {
		try {
			properties.load(getServletContext().getResourceAsStream("/WEB-INF/subscription-manager.properties"));
		} catch (Exception e) {
			LOG.error("Failed to read config file, aborting");
			LOG.debug("{}", e);
			throw new ServletException(e);
		}

		/* set default subscription timeout */
		if (properties.containsKey(PROP_KEY_TIMEOUT))
			try {
				subscriptionTimeout = Integer.valueOf(properties.getProperty(PROP_KEY_TIMEOUT));
				LOG.info("Subscription timeout is {} s", subscriptionTimeout);
			} catch (Exception e) {
				LOG.info("Invalid subscription timeout in properties, using default", e);
			}

		MessageCache.setProperties(properties);
		try {
			cache = MessageCache.getInstance();
		} catch (ClassNotFoundException | SQLException e) {
			LOG.error("Failed to instantiate message cache, aborting");
			LOG.debug("{}", e);
			throw new ServletException(e);
		}

		subscriptionExpirationTimer.schedule(new SubscriptionExpirationTimerTask(cache), 0, MILLIS_PER_MINUTE);
	}

	@Override
	public void destroy() {
		subscriptionExpirationTimer.cancel();
		super.destroy();
	}

	private static class SubscriptionExpirationTimerTask extends TimerTask {
		private final MessageCache cache;

		public SubscriptionExpirationTimerTask(MessageCache cache) {
			this.cache = cache;
		}

		public void run() {
			LOG.debug("Purging expired subscriptions");
			try {
				cache.purgeExpiredSubscriptions();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				LOG.debug("{}", e);
			}
		}
	}
}
