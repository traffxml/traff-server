-- Drop existing tables and indices
drop index if exists filter_index;
drop index if exists message_expires_index;
drop index if exists message_bbox_index;
drop index if exists message_roadClass_index;
drop index if exists message_firstFeed_index;
drop table if exists filter;
drop table if exists subscription;
drop table if exists message;
drop table if exists feed;
drop table if exists version;
-- Subscription
-- TODO default for expiration should be
-- (CURRENT_TIMESTAMP AT TIME ZONE INTERVAL '0:00' HOUR TO MINUTE + INTERVAL 1 hour)
-- but that is not supported in HSQL as of Dec 2020 (was introduced in 2.3.3 but broken in later
-- versions, expected to be back in the next release)
create table subscription(id varchar(255) primary key not null,
  lastFeed bigint default 0 not null,
  timeout interval day(3) to second(0) default '0 1:00:00' not null,
  expires timestamp(0) with time zone default CURRENT_TIMESTAMP not null);
create index subscription_expires_index on subscription(expires);
-- Filter
create table filter(subscriptionId varchar(255) not null,
  minRoadClass varchar(16),
  minLat float,
  minLon float,
  maxLat float,
  maxLon float,
  foreign key(subscriptionId) references subscription(id) on delete cascade);
create index filter_index on filter(subscriptionId, minRoadClass, minLat, minLon, maxLat, maxLon);
-- Message
create table message(id varchar(255) primary key not null,
  expires timestamp(0) not null,
  roadClass varchar(16),
  minLat float not null,
  minLon float not null,
  maxLat float not null,
  maxLon float not null,
  firstFeed bigint not null,
  data clob not null);
create index message_expires_index on message(expires);
create index message_bbox_index on message(minLat, minLon, maxLat, maxLon);
create index message_roadClass_index on message(roadClass);
create index message_firstFeed_index on message(firstFeed);
-- Feed
create table feed(feed bigint not null);
insert into feed(feed) values(1);
-- Version
create table version(name varchar(255) primary key, version integer);
insert into version(name, version) values('traff', 2);
-- Roles
create role traff_receiver;
create role subscription_manager;
grant select, insert, update, delete on table subscription to subscription_manager;
grant select, insert, update, delete on table filter to subscription_manager;
grant select on table message to subscription_manager;
-- subscription_manager has no delete permission on message and thus cannot purge expired messages
grant select, insert, update, delete on table message to traff_receiver;
grant select, update on table feed to subscription_manager;
grant select on table feed to traff_receiver;
grant select on table version to subscription_manager, traff_receiver;
-- Users
-- TODO set password
create user SM1 password '';
grant subscription_manager to SM1;
-- TODO set password
create user TR1 password '';
grant traff_receiver to TR1;
