/*
 * Copyright © 2017–2020 Michael von Glasow.
 * 
 * This file is part of TraFF Server.
 *
 * TraFF Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TraFF Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TraFF Server.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traffserver.core.messagecache;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.lang.ref.WeakReference;
import java.security.SecureRandom;
import java.sql.Array;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.traff.BoundingBox;
import org.traffxml.traff.TraffFeed;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.subscription.FilterItem;
import org.traffxml.traff.subscription.Subscription;

/* NOTE: 
 * This is a portable class which should be kept clean of any dependencies on platform-specific
 * classes (JRE or Android).
 */

/**
 * @brief Handles storage, retrieval and expiration of TraFF messages.
 * 
 * This class is a weak singleton, which has the following implications:
 * <ul>
 * <li>This class is never instantiated directly. Rather, an instance is obtained by calling
 * {@link #getInstance()}.</li>
 * <li>At any given time, there can never be more than one instance of this class.</li>
 * <li>The instance may be garbage-collected after the last reference to it is deleted. In that
 * case, the next call to {@link #getInstance()} will create a new instance.</li>
 * </ul>
 * 
 * The message cache can be configured by calling {@link #setProperties(Properties)} before an
 * instance is obtained. Properties not starting with {@code cache.} are ignored, thus the
 * properties can be shared with other components as long as the {@code cache.} prefix is reserved
 * for the message cache.
 */
public final class MessageCache {
	/**
	 * @brief The logger for log output.
	 * 
	 * Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	/**
	 * Identifier for TraFF schema version in version table
	 */
	private static final String DB_VERSION_NAME_TRAFF = "traff";

	/** 
	 * SQL statement to query version information.
	 */
	private static final String DB_VERSION_QUERY_STMT =
			"select version from version where name = ?;";

	/** 
	 * DB schema version for persistent storage of cached messages, incremented every time the schema changes.
	 * 
	 * You may wish to store this value in (or along with) your database and compare it on startup.
	 * If the versions do not match, reinitialize your local database. 
	 */
	private static final int DB_VERSION_MESSAGE_CACHE = 2;

	/**
	 * SQL statement for poll queries
	 */
	private static final String POLL_QUERY_STMT = 
			"select distinct message.*\n" + 
					"from message, filter, subscription\n" + 
					"where subscription.id = ?\n" + 
					"  and filter.subscriptionId = subscription.id\n" + 
					"  and message.firstFeed > subscription.lastFeed\n" + 
					"  and ( -- road class matches\n" + 
					"    filter.minRoadClass is null\n" + 
					"    or filter.minRoadClass = 'OTHER'\n" + 
					"    or message.roadClass is null\n" + 
					"    or filter.minRoadClass = message.roadClass\n" + 
					"    or (filter.minRoadClass = 'TRUNK' and message.roadClass = 'MOTORWAY')\n" + 
					"    or (filter.minRoadClass = 'PRIMARY' and message.roadClass in ('MOTORWAY', 'TRUNK'))\n" + 
					"    or (filter.minRoadClass = 'SECONDARY' and message.roadClass in ('MOTORWAY', 'TRUNK', 'PRIMARY'))\n" + 
					"    or (filter.minRoadClass = 'TERTIARY' and message.roadClass in ('MOTORWAY', 'TRUNK', 'PRIMARY', 'SECONDARY'))\n" + 
					"  )\n" + 
					"  and ( -- bbox matches\n" + 
					"    filter.minLat is null\n" + 
					"    -- bboxes share a pole\n" + 
					"    or (filter.minLat = -90 and message.minLat = -90)\n" + 
					"    or (filter.maxLat = 90 and message.maxLat = 90)\n" + 
					"    or ( -- general case: latitude ranges must overlap\n" + 
					"      (not (filter.minLat > message.maxLat or filter.maxLat < message.minLat))\n" + 
					"      and ( -- examine only longitudes\n" + 
					"        -- bboxes share the 180 degree meridian\n" + 
					"        ((abs(filter.minLon) = 180 or abs(filter.maxLon) = 180) and (abs(message.minLon) = 180 or abs(message.maxLon) = 180))\n" + 
					"        -- neither bbox crosses the 180 degree meridian\n" + 
					"        or (filter.minLon <= filter.maxLon\n" + 
					"          and message.minLon <= message.maxLon\n" + 
					"          and not (filter.minLon > message.maxLon or filter.maxLon < message.minLon)\n" + 
					"        )\n" + 
					"        -- both bboxes cross (and thus share) the 180 degree meridian\n" + 
					"        or (filter.minLon > filter.maxLon and message.minLon > message.maxLon)\n" + 
					"        -- filter crosses the 180 degree meridian but message does not\n" + 
					"        or (filter.minLon > filter.maxLon\n" + 
					"          and message.minLon <= message.maxLon\n" + 
					"          and not (filter.minLon > message.maxLon and filter.maxLon < message.minLon)\n" + 
					"        )\n" + 
					"        -- filter does not cross the 180 degree meridian but message does\n" + 
					"        or (filter.minLon <= filter.maxLon\n" + 
					"          and message.minLon > message.maxLon\n" + 
					"          and not (filter.minLon > message.maxLon and filter.maxLon < message.minLon)\n" + 
					"        )\n" + 
					"      )\n" + 
					"    )\n" + 
					"  );";

	/**
	 * A second in milliseconds.
	 */
	private static final int MILLIS_TO_SECONDS = 1000;

	/**
	 * Separator character for property keys.
	 */
	private static final String PROP_KEY_SEPARATOR = ".";

	/**
	 * Prefix for properties parsed by the message cache.
	 */
	private static final String PROP_KEY_CACHE_PREFIX = "cache" + PROP_KEY_SEPARATOR;

	/**
	 * Property key for the JDBC database driver.
	 * 
	 * If left empty, the message cache will try to find a suitable driver.
	 */
	private static final String PROP_KEY_DB_DRIVER = PROP_KEY_CACHE_PREFIX + "dbDriver";

	/**
	 * Property key for the JDBC URL to the database.
	 */
	private static final String PROP_KEY_DB_URL = PROP_KEY_CACHE_PREFIX + "dbUrl";

	/**
	 * Property key for the database user.
	 * 
	 * Some databases may not require a user name. Others may allow the user name to be specified
	 * either as part of the URL or separately. Where this is the case, avoid specifying both as it
	 * is up to the driver to decide which credentials to use.
	 */
	private static final String PROP_KEY_DB_USER = PROP_KEY_CACHE_PREFIX + "dbUser";

	private static WeakReference<MessageCache> instance;

	/**
	 * Properties for the message cache.
	 */
	private static Properties properties = new Properties();

	/** The JDBC connection to the database */
	private Connection connection;

	/**
	 * @brief Obtains an instance of the MessageCache.
	 * 
	 * By default, the MessageCache will operate on an in-memory database, which is lost when the
	 * MessageCache is deinstantiated. To make data persistent, call {@link #setDatabaseUrl(String)}
	 * or {@link #setDatabasePath(File)} before the first call to this method.
	 * 
	 * @return The instance
	 * 
	 * @throws SQLException If any of the database operations fails
	 * @throws ClassNotFoundException If the HSQLDB JDBC driver cannot be found
	 */
	public static synchronized MessageCache getInstance() throws ClassNotFoundException, SQLException {
		MessageCache strongInstance = null;

		if (instance != null) {
			strongInstance = instance.get();
			if (strongInstance != null)
				return strongInstance;
		}

		// need to (re-)instantiate
		strongInstance = new MessageCache();
		instance = new WeakReference<MessageCache>(strongInstance);
		return strongInstance;
	}

	/**
	 * @brief Sets configuration properties for the message cache.
	 * 
	 * This method must be called before the first call to {@link #getInstance()}, else an
	 * {@link IllegalStateException} will be thrown.
	 * 
	 * Only HSQLDB is fully supported as a database. Other DBMSes may work but are untested.
	 * 
	 * @param props The properties to set.
	 */
	public static synchronized void setProperties(Properties props) {
		/* find out if we are instantiated, and throw an exception if we are */
		MessageCache strongInstance = null;

		if (instance != null) {
			strongInstance = instance.get();
			if (strongInstance != null)
				throw new IllegalStateException("Database URL cannot be changed once an instance has been created");
		}

		properties = props;
	}

	/**
	 * @brief Changes an existing subscription in the database.
	 * 
	 * Changing a subscription means giving it a new filter list, discarding the previous one.
	 * 
	 * @param id The identifier for the subscription to change
	 * @param filterList The new filter list for the subscription
	 * 
	 * @return A {@link Subscription} with the ID assigned by the system and the caller-supplied
	 * filter list
	 * 
	 * @throws IllegalArgumentException if {@code id} does not match a known subscription ID
	 * @throws SQLException If the subscription could not be modified in the database
	 */
	public Subscription changeSubscription(String id, List<FilterItem> filterList)
			throws IllegalArgumentException, SQLException {
		if (!isValidSubscription(id))
			throw new IllegalArgumentException(String.format("%s is not a valid subscription", id));
		touchSubscriptionInternal(id);
		PreparedStatement stmt1 = connection.prepareStatement("update subscription set lastFeed = 0 where id = ?");
		stmt1.setString(1, id);
		stmt1.execute();
		stmt1.close();
		PreparedStatement stmt2 = connection.prepareStatement("delete from filter where subscriptionId = ?");
		stmt2.setString(1, id);
		stmt2.execute();
		stmt2.close();
		addFilterList(id, filterList);
		if (!connection.getAutoCommit())
			connection.commit();
		return new Subscription(id, filterList);
	}

	/**
	 * @brief Whether a given ID refers to an existing subscription.
	 * 
	 * @param id A subscription ID
	 * 
	 * @return True if a subscription with that ID exists, false if not.
	 * @throws SQLException 
	 */
	public boolean isValidSubscription(String id) throws SQLException {
		PreparedStatement stmt = connection.prepareStatement("select * from subscription where id = ?;");
		stmt.setString(1, id);
		ResultSet rset = stmt.executeQuery();
		boolean res = rset.next();
		stmt.close();
		rset.close();
		return res;
	}

	/**
	 * @brief Polls the cache for new messages matching the subscription.
	 * 
	 * Polling the cache will return all messages matching at least one filter item in the filter
	 * list of the subscription for the first poll after a subscription has been set up or changed.
	 * For subsequent subscription, the selection is further limited to messages which have changed
	 * since the last poll operation.
	 * 
	 * Internally, a poll operation first ensures that the {@code feed} counter is bigger than the
	 * highest {@code firstFeed} of any message returned, before any messages are retrieved.
	 * 
	 * After retrieving the messages, the {@code lastFeed} column for the subscription is set to
	 * the highest {@code firstFeed} observed among the messages returned.
	 * 
	 * Maintaining this order is important to prevent race conditions between poll and update
	 * operations taking place concurrently, ensuring no messages are skipped.
	 * 
	 * @param id The subscription identifier
	 * 
	 * @return A feed of all messages matching the criteria
	 * 
	 * @throws IllegalArgumentException if {@code id} does not match a known subscription ID
	 * @throws SQLException
	 */
	public TraffFeed poll(String id)
			throws IllegalArgumentException, SQLException {
		if (!isValidSubscription(id))
			throw new IllegalArgumentException(String.format("%s is not a valid subscription", id));
		touchSubscriptionInternal(id);
		PreparedStatement stmtFeed = connection.prepareStatement(
				"update feed set feed = (select max(firstFeed) + 1 from message) " +
				"where feed <= (select max(firstFeed) from message);");
		stmtFeed.execute();
		stmtFeed.close();

		long feed = 0;
		List<TraffMessage> messages = new ArrayList<TraffMessage>();
		PreparedStatement stmtMsg = connection.prepareStatement(POLL_QUERY_STMT);
		stmtMsg.setString(1, id);
		ResultSet rset = stmtMsg.executeQuery();
		while (rset.next()) {
			try {
				long firstFeed = rset.getLong("firstFeed");
				if (firstFeed > feed)
					feed = firstFeed;
				Clob clob = rset.getClob("data");
				messages.add(TraffMessage.read(clob.getSubString(1, (int) clob.length())));
			} catch (Exception e) {
				LOG.debug("{}", e);
			}
		}
		stmtMsg.close();
		rset.close();

		PreparedStatement stmtSub = connection.prepareStatement("update subscription set lastFeed = ? where id = ?");
		stmtSub.setLong(1, feed);
		stmtSub.setString(2, id);
		stmtSub.execute();
		stmtSub.close();

		if (!connection.getAutoCommit())
			connection.commit();
		return new TraffFeed(messages);
	}

	/**
	 * @brief Processes a collection of new TraFF messages.
	 * 
	 * Messages which have already expired are discarded immediately.
	 * 
	 * Then, for each new message, existing messages which will be overridden are read from the database.
	 * Together with the bounding box of the new message location, they are used to determine the
	 * bounding box for the new message. Overridden messages with a different ID are deleted and the
	 * previous version of the new message is update, or the new message is added to the database if no
	 * previous version exists. The {@code firstFeed} column of the new (or updated) message is set to
	 * the value of {@code feed}.
	 * 
	 * TODO The road class is currently taken from the new message. Values in old messages are ignored.
	 * 
	 * @param newMessages The new message
	 * 
	 * @throws SQLException 
	 */
	public void processUpdate(Collection<TraffMessage> newMessages) throws SQLException {
		/* drop expired messages */
		Date now = new Date();
		Set<TraffMessage> expired = new TreeSet<TraffMessage>();
		for (TraffMessage message : newMessages)
			if (message.isExpired(now))
				expired.add(message);
		newMessages.removeAll(expired);

		for (TraffMessage message : newMessages) {
			List<String> ids = new ArrayList<String>();
			ids.add(message.id);
			ids.addAll(Arrays.asList(message.replaces));
			PreparedStatement stmtQ = connection.prepareStatement("select * from message where id in (unnest(?))");
			Array idArray = stmtQ.getConnection().createArrayOf("VARCHAR", ids.toArray(new String[]{}));
			stmtQ.setArray(1, idArray);
			ResultSet rset = stmtQ.executeQuery();
			BoundingBox bbox = null;
			if (message.location != null)
				bbox = message.location.getBoundingBox();
			boolean update = false;
			boolean delete = false;
			while (rset.next()) {
				if (message.id.equals(rset.getString("id")))
					update = true;
				else
					delete = true;
				if (bbox == null)
					bbox = new BoundingBox(rset.getFloat("minLat"), rset.getFloat("minLon"),
							rset.getFloat("maxLat"), rset.getFloat("maxLon"));
				else
					bbox = bbox.merge(new BoundingBox(rset.getFloat("minLat"), rset.getFloat("minLon"),
							rset.getFloat("maxLat"), rset.getFloat("maxLon")));
				// TODO road class?
			}
			stmtQ.close();
			rset.close();

			if (delete) {
				PreparedStatement stmtD = connection.prepareStatement("delete from message where id in (unnest(?))");
				Array delIdArray = stmtD.getConnection().createArrayOf("VARCHAR", message.replaces);
				stmtD.setArray(1, delIdArray);
				stmtD.execute();
				stmtD.close();
			}

			PreparedStatement stmtU = connection.prepareStatement(update
					? "update message set (expires, roadClass, minLat, minLon, maxLat, maxLon, firstFeed, data) = (?, ?, ?, ?, ?, ?, (select max(feed) from feed), ?) where id = ?;"
							: "insert into message(expires, roadClass, minLat, minLon, maxLat, maxLon, firstFeed, data, id) values(?, ?, ?, ?, ?, ?, (select max(feed) from feed), ?, ?);");
			java.sql.Date expires = new java.sql.Date(message.getEffectiveExpirationTime().getTime());
			stmtU.setDate(1, expires);
			// TODO null locations only happen for cancellations, which should inherit the previous roadClass
			if ((message.location == null) || (message.location.roadClass == null))
				stmtU.setNull(2, Types.VARCHAR);
			else
				stmtU.setString(2, message.location.roadClass.name());
			stmtU.setFloat(3, bbox.minLat);
			stmtU.setFloat(4, bbox.minLon);
			stmtU.setFloat(5, bbox.maxLat);
			stmtU.setFloat(6, bbox.maxLon);
			Clob data = stmtU.getConnection().createClob();
			try {
				data.setString(1, message.toXml());
				stmtU.setClob(7, data);
				stmtU.setString(8, message.id);
				stmtU.execute();
			} catch (Exception e) {
				LOG.warn("Unable to update message {}", message.id);
				LOG.debug("{}", e);
			} finally {
				data.free();
				stmtU.close();
			}
		}

		if (!connection.getAutoCommit())
			connection.commit();
	}

	/**
	 * @brief Purges expired messages from the cache.
	 * 
	 * @throws SQLException
	 */
	public void purgeExpiredMessages() throws SQLException {
		Date javaDate = new Date();
		java.sql.Date sqlDate = new java.sql.Date(javaDate.getTime());
		PreparedStatement stmt = connection.prepareStatement("delete from message where expires < ?;");
		stmt.setDate(1, sqlDate);
		stmt.execute();
		stmt.close();
		stmt = connection.prepareStatement("checkpoint defrag;");
		stmt.execute();
		stmt.close();
		if (!connection.getAutoCommit())
			connection.commit();
	}

	/**
	 * Purges expired subscriptions from the cache.
	 * 
	 * @throws SQLException
	 */
	public void purgeExpiredSubscriptions() throws SQLException {
		Date javaDate = new Date();
		java.sql.Date sqlDate = new java.sql.Date(javaDate.getTime());
		PreparedStatement stmt = connection.prepareStatement("delete from subscription where expires < ?;");
		stmt.setDate(1, sqlDate);
		stmt.execute();
		stmt.close();
		if (!connection.getAutoCommit())
			connection.commit();
	}

	/**
	 * @brief Retrieves a list of messages currently in the cache.
	 * 
	 * If {@code source} is specified, only messages from that source will be retrieved; otherwise
	 * all messages will be returned.
	 * 
	 * The return value is a list of messages, not a feed, as results are intended for internal use.
	 * 
	 * @param source The source identifier, without a trailing separator character
	 * 
	 * @return A list of messages matching the criteria
	 * 
	 * @throws SQLException
	 */
	public List<TraffMessage> query(String source) throws SQLException {
		List<TraffMessage> messages = new ArrayList<TraffMessage>();
		PreparedStatement stmt;
		if (source == null)
			stmt = connection.prepareStatement("select * from message;");
		else
			stmt = connection.prepareStatement(
					String.format("select * from message where id like '%s:%%'", source));
		ResultSet rset = stmt.executeQuery();
		while (rset.next()) {
			Clob clob = rset.getClob("data");
			try {
				messages.add(TraffMessage.read(clob.getSubString(1, (int) clob.length())));
			} catch (Exception e) {
				LOG.debug("{}", e);
			} finally {
				clob.free();
			}
		}
		stmt.close();
		rset.close();

		if (!connection.getAutoCommit())
			connection.commit();
		return messages;
	}

	/**
	 * @brief Adds a new subscription to the database.
	 * 
	 * @param filterList The filter list for the new subscription
	 * @param timeout The subscription timeout, in seconds
	 * 
	 * @return A {@link Subscription} with the ID assigned by the system and the caller-supplied
	 * filter list
	 * 
	 * @throws SQLException If the subscription could not be entered into the database
	 */
	public Subscription subscribe(List<FilterItem> filterList, int timeout) throws SQLException {
		/* Generate a random 128-bit ID using a secure random generator */
		SecureRandom random = new SecureRandom();
		byte idBytes[] = new byte[16];
		random.nextBytes(idBytes);
		String id = "";
		for (byte idByte : idBytes)
			id = String.format("%s%02x", id, idByte);

		PreparedStatement stmt = connection.prepareStatement("insert into subscription(id, timeout, expires) values(?, ?, ?)");
		stmt.setString(1, id);
		stmt.setInt(2, timeout);
		stmt.setDate(3, new java.sql.Date(new Date().getTime() + timeout * MILLIS_TO_SECONDS));
		stmt.execute();
		stmt.close();
		addFilterList(id, filterList);
		if (!connection.getAutoCommit())
			connection.commit();
		return new Subscription(id, filterList);
	}

	/**
	 * Updates the last activity timestamp of a subscription to prevent it from expiring.
	 * 
	 * @param id The identifier for the subscription to update
	 * @throws SQLException If the subscription could not be updated
	 * @throws IllegalArgumentException if {@code id} does not match a known subscription ID
	 */
	public void touchSubscription(String id) throws IllegalArgumentException, SQLException {
		if (!isValidSubscription(id))
			throw new IllegalArgumentException(String.format("%s is not a valid subscription", id));
		touchSubscriptionInternal(id);
		if (!connection.getAutoCommit())
			connection.commit();
	}

	/**
	 * @brief Removes a subscription from the database.
	 * 
	 * @param id The identifier for the subscription to remove
	 * 
	 * @throws SQLException If the subscription could not be removed (including cases in which the
	 * id does not refer to an existing subscription)
	 */
	public void unsubscribe(String id) throws SQLException {
		PreparedStatement stmt = connection.prepareStatement("delete from subscription where id = ?");
		stmt.setString(1, id);
		stmt.execute();
		stmt.close();
		if (!connection.getAutoCommit())
			connection.commit();
	}

	/**
	 * @brief Instantiates a new MessageCache.
	 * 
	 * This constructor is not intended to be called directly. Instead, obtain an instance by
	 * calling {@link #getInstance()}.
	 * 
	 * @throws ClassNotFoundException if the JDBC driver is not found
	 * @throws SQLException if the database connection, or initialization, fails
	 */
	private MessageCache() throws SQLException, ClassNotFoundException {
		PreparedStatement stmt;

		/* make sure the driver is loaded if it is specified */
		if (properties.containsKey(PROP_KEY_DB_DRIVER)) {
			LOG.info("Loading JDBC driver {}",
					properties.getProperty(PROP_KEY_DB_DRIVER));
			Class.forName(properties.getProperty(PROP_KEY_DB_DRIVER));
		}

		/* prepare DB connection and dbInfo */
		if (properties.containsKey(PROP_KEY_DB_URL)) {
			if (properties.containsKey(PROP_KEY_DB_USER))
				LOG.info("Connecting to database {} as user {}",
						properties.getProperty(PROP_KEY_DB_URL),
						properties.getProperty(PROP_KEY_DB_USER));
			else
				LOG.info("Connecting to database {}",
						properties.getProperty(PROP_KEY_DB_URL));
		} else
			LOG.warn("No database URL specified, using default");
		connection = DriverManager.getConnection(
				properties.getProperty(PROP_KEY_DB_URL, "jdbc:hsqldb:mem:."),
				properties.getProperty(PROP_KEY_DB_USER),
				null);
		connection.setAutoCommit(false);

		/* query the version table */
		stmt = connection.prepareStatement(DB_VERSION_QUERY_STMT);
		stmt.setString(1, DB_VERSION_NAME_TRAFF);
		ResultSet rset = stmt.executeQuery();
		int version = 0;
		while (rset.next())
			version = rset.getInt("version");

		/* throw an error if DB is outdated */
		/*
		 * TODO
		 * Right now this accepts versions higher than the required one, which may be unsafe if a
		 * future schema version introduces breaking changes. It would be better to maintain two
		 * version values:
		 * - target version, i.e. the version of the schema specification which the DB follows and
		 *   which the API library needs to support in order to make use of all features
		 * - minimum version, i.e. the schema version which the API library needs to support in
		 *   order to read from the database and write to it without corrupting it
		 * An API library which supports the minimum version (or higher) but not the target version
		 * will work correctly but may not be able to make use of all functionalities offered by
		 * the target schema version.
		 */
		if (version < DB_VERSION_MESSAGE_CACHE)
			throw new IllegalStateException(String.format("database is version %d, expected %d",
					version, DB_VERSION_MESSAGE_CACHE));
	}

	/**
	 * @brief Adds the filter list for a new or changed subscription.
	 * 
	 * If the subscription already exists, the caller is responsible for deleting any existing subscriptions.
	 * 
	 * @param subscriptionId The identifier for the subscription
	 * @param filterList The new filter list
	 * 
	 * @throws SQLException
	 */
	private void addFilterList(String subscriptionId, List<FilterItem> filterList) throws SQLException {
		for (FilterItem item : filterList) {
			PreparedStatement stmt = connection.prepareStatement("insert into filter(subscriptionId, minRoadClass, minLat, minLon, maxLat, maxLon) values(?, ?, ?, ?, ?, ?)");
			stmt.setString(1, subscriptionId);
			if (item.minRoadClass != null)
				stmt.setString(2, item.minRoadClass.name());
			else
				stmt.setNull(2, java.sql.Types.VARCHAR);
			if (item.bbox != null) {
				stmt.setFloat(3, item.bbox.minLat);
				stmt.setFloat(4, item.bbox.minLon);
				stmt.setFloat(5, item.bbox.maxLat);
				stmt.setFloat(6, item.bbox.maxLon);
			} else {
				stmt.setNull(3, java.sql.Types.FLOAT);
				stmt.setNull(4, java.sql.Types.FLOAT);
				stmt.setNull(5, java.sql.Types.FLOAT);
				stmt.setNull(6, java.sql.Types.FLOAT);
			}
			stmt.execute();
			stmt.close();
		}
	}

	/**
	 * Updates the last activity timestamp of a subscription to prevent it from expiring.
	 * 
	 * <p>This is the internal implementation which updates the database contents but does not commit the
	 * transaction. It is intended for use by {@code MessageCache} methods which need to update the timestamp
	 * as part of larger transactions. It performs no verification if {@code id} actually refers to an
	 * existing subscription, and does not commit the transaction if autocommit is not active.
	 * 
	 * To merely update the timestamp without any other database operations, call
	 * {@link #touchSubscription(String)} instead, which is a wrapper around this function. It verifies if
	 * the subscription actually exists, and commits the transaction as needed.
	 * 
	 * @param id The identifier for the subscription to update
	 * @throws SQLException If the subscription could not be updated
	 */
	private void touchSubscriptionInternal(String id) throws SQLException {
		PreparedStatement stmt = connection.prepareStatement("update subscription set expires = CURRENT_TIMESTAMP + timeout where id = ?");
		stmt.setString(1, id);
		stmt.execute();
		stmt.close();
	}
}
