/*
 * Copyright © 2017–2020 Michael von Glasow.
 * 
 * This file is part of TraFF Server.
 *
 * TraFF Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TraFF Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TraFF Server.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traffserver.core.traffreceiver;

import java.io.File;
import java.io.FileReader;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.input.DataSource;
import org.traffxml.traffserver.core.messagecache.MessageCache;

public class TraffReceiver {
	/**
	 * @brief The logger for log output.
	 * 
	 * Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	/** The poll interval in seconds, if nothing else is configured explicitly */
	private static final int DEFAULT_POLL_INTERVAL = 600;

	/** Milliseconds per minute */
	private static final int MILLIS_PER_MINUTE = 60000;

	/** The available data sources */
	private static Collection<DataSource> sources = new ArrayList<DataSource>();

	/** All currently configured source IDs */
	private static Set<String> sourceIds = new HashSet<String>();

	/** The TraFF message cache */
	private static MessageCache cache = null;

	/** The timer to purge expired messages from the cache */
	private static Timer messageExpirationTimer = new Timer("messageExpirationTimer", true);

	/** The timer to poll sources for updates */
	private static Timer pollTimer = new Timer("pollTimer", false);

	/** The configuration for the TraFF receiver */
	private static Properties receiverProperties = new Properties();

	private static String getParam(String param, String[] args, int pos) {
		if(pos >= args.length) {
			System.out.println("-" + param + " needs an argument.");
			System.exit(1);
		}
		return args[pos];
	}

	/**
	 * @brief Returns the interval at which the source will be polled.
	 * 
	 * The poll interval is the interval set in the {@link Const#KEY_SOURCE_INTERVAL} property for
	 * the source, or {@link #DEFAULT_POLL_INTERVAL} if the former is not set or invalid. However,
	 * it is never less than {@link DataSource#getMinUpdateInterval()} for the source.
	 * 
	 * @param source The source
	 * 
	 * @return The update interval in seconds.
	 */
	private static int getPollInterval(DataSource source) {
		int interval = DEFAULT_POLL_INTERVAL;
		try {
			interval = Integer.valueOf(receiverProperties.getProperty(
					Const.getSourcePrefKey(source.getId(), Const.KEY_SOURCE_INTERVAL)));
		} catch (Exception e) {
			// NOP
		}
		interval = Math.max(interval, source.getMinUpdateInterval());
		return interval;
	}

	private static void printUsage() {
		System.out.println("Arguments:");
		System.out.println("  -config <path>   Use config file in <path>");
		System.out.println();
	}

	public static void main(String[] args) {
		String configPath = null;
		if (args.length == 0) {
			printUsage();
			System.exit(1);
		}
		for (int i = 0; i < args.length; i++) {
			if ("-config".equals(args[i])) {
				configPath = getParam("config", args, ++i);
			} else {
				LOG.error("Unknown argument: " + args[i]);
				printUsage();
				System.exit(0);
			}
		}
		if (configPath == null) {
			LOG.error("No config file specified, aborting");
			System.exit(1);
		} else {
			File configFile = new File(configPath);
			if (!configFile.exists()) {
				LOG.error("Config file not found, aborting");
				System.exit(1);
			} else if (!configFile.isFile()) {
				LOG.error("Config file is not a file, aborting");
				System.exit(1);
			} else if (!configFile.canRead()) {
				LOG.error("Config file is not readable, aborting");
				System.exit(1);
			}
		}

		Runtime.getRuntime().addShutdownHook(new Thread("shutdownHook") {
			@Override
			public void run() {
				LOG.info("Received shutdown request!");
				if (pollTimer != null)
					pollTimer.cancel();
				pollTimer = null;
				// TODO dump cache if configured (see RoadEagle, TraffReceiver#closeInput())
			}
		});

		try {
			receiverProperties.load(new FileReader(configPath));
		} catch (Exception e) {
			LOG.error("Failed to read config file, aborting");
			LOG.debug("{}", e);
			System.exit(1);
		}

		MessageCache.setProperties(receiverProperties);
		try {
			cache = MessageCache.getInstance();
		} catch (ClassNotFoundException | SQLException e) {
			LOG.error("Failed to instantiate message cache, aborting");
			LOG.debug("{}", e);
			System.exit(1);
		}

		/* Harvest source IDs from properties */
		for (String key : receiverProperties.stringPropertyNames()) {
			Matcher matcher = Const.KEY_SOURCE_PATTERN.matcher(key);
			if (matcher.find())
				sourceIds.add(matcher.group(2));
		}

		messageExpirationTimer.schedule(new MessageExpirationTimerTask(cache), 0, MILLIS_PER_MINUTE);

		for (String id : sourceIds) {
			String driver = receiverProperties.getProperty(Const.getSourcePrefKey(id, Const.KEY_SOURCE_DRIVER));
			if (driver == null) {
				LOG.warn("No driver specified for source {}, skipping", id);
				continue;
			}
			DataSource source;
			try {
				Class<?> clazz = Class.forName(driver);
				Class<? extends DataSource> subclazz = clazz.asSubclass(DataSource.class);
				Constructor<? extends DataSource> constr = subclazz.getConstructor(String.class, String.class, Properties.class);
				source = constr.newInstance(id,
						receiverProperties.getProperty(Const.getSourcePrefKey(id, Const.KEY_SOURCE_URL)),
						receiverProperties);
			} catch (Exception e) {
				LOG.warn("Could not load driver class {} for source {}, skipping", driver, id);
				LOG.debug("{}", e);
				continue;
			}
			sources.add(source);

			if (!Boolean.parseBoolean(receiverProperties.getProperty(Const.getSourcePrefKey(id, Const.KEY_SOURCE_ENABLED), "false")))
				continue;

			/* Schedule poll tasks if source is enabled */
			pollTimer.schedule(new PollTimerTask(source), 0, getPollInterval(source) * 1000);

			boolean debugInput = Boolean.parseBoolean(receiverProperties.getProperty(Const.KEY_DEBUG_INPUT, "false"));

			DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.ROOT);
			fmt.setTimeZone(TimeZone.getTimeZone("UTC"));
			// TODO //debugSessionName = fmt.format(new Date(System.currentTimeMillis()));
			// TODO //File dumpDir = TraffReceiver.this.getExternalFilesDir(null);
		}
		LOG.info("Done adding sources");
	}

	private static class MessageExpirationTimerTask extends TimerTask {
		private final MessageCache cache;

		public MessageExpirationTimerTask(MessageCache cache) {
			this.cache = cache;
		}

		public void run() {
			LOG.debug("Purging expired messages");
			try {
				cache.purgeExpiredMessages();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				LOG.debug("{}", e);
			}
		}
	}

	private static class PollTimerTask extends TimerTask {
		DataSource source;

		private PollTimerTask(DataSource source) {
			this.source = source;
		}

		@Override
		public void run() {
			LOG.debug("Enter poll timer task for source {}" , source.getId());
			if (!Boolean.parseBoolean(receiverProperties.getProperty(Const.getSourcePrefKey(source.getId(), Const.KEY_SOURCE_ENABLED), "false")))
				return;

			/* poll source */
			LOG.debug("Polling source {}" , source.getId());
			try {
				Collection<TraffMessage> oldMessages = null;
				if (source.needsExistingMessages())
					oldMessages = cache.query(source.getId());
				Collection <TraffMessage> newMessages = source.poll(oldMessages, getPollInterval(source));
				if (newMessages != null)
					cache.processUpdate(newMessages);
			} catch (Exception e) {
				LOG.warn("Polling source %s failed with %s", source.getId(), e.getClass().getSimpleName());
				LOG.debug("{}", e);
			}
			LOG.debug("Done polling source {}" , source.getId());
		}
	}
}
