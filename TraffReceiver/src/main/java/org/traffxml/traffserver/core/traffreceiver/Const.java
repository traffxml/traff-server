/*
 * Copyright © 2017–2020 Michael von Glasow.
 * 
 * This file is part of TraFF Server.
 *
 * TraFF Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TraFF Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TraFF Server.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traffserver.core.traffreceiver;

import java.util.regex.Pattern;

// FIXME move to a shared location
/**
 * Constants used throughout the application
 */
public class Const {
	/**
	 * Property key for input debug setting.
	 */
	public static final String KEY_CACHE_LTDB = "cache.ltdb";

	/**
	 * Property key for input debug setting.
	 */
	public static final String KEY_DEBUG_INPUT = "debug.input";

	/**
	 * Property key for output debug setting.
	 */
	public static final String KEY_DEBUG_OUTPUT = "debug.output";

	/**
	 * Property key for verbose logging.
	 */
	public static final String KEY_DEBUG_VERBOSE = "debug.verbose";

	/**
	 * Property key for log mirroring.
	 */
	public static final String KEY_DEBUG_MIRROR = "debug.mirror";

	/**
	 * Property key prefix for individual sources.
	 */
	public static final String KEY_SOURCE_PREFIX = "source";

	/**
	 * Property key suffix for the setting which chooses the driver for the source.
	 */
	public static final String KEY_SOURCE_DRIVER = "driver";

	/**
	 * Property key suffix for the setting which enables the source.
	 */
	public static final String KEY_SOURCE_ENABLED = "enabled";

	/**
	 * Property key suffix for the setting which determines the poll interval for the source.
	 */
	public static final String KEY_SOURCE_INTERVAL = "interval";

	/**
	 * Property key suffix for the setting which chooses the URL for the source.
	 */
	public static final String KEY_SOURCE_URL = "url";

	/**
	 * @brief Pattern which matches properties referring to a source.
	 * 
	 * This is used to “harvest” source IDs from the properties.
	 */
	public static final Pattern KEY_SOURCE_PATTERN = Pattern.compile("(source\\[)(.*?)(\\]\\..*)");

	public static String getSourcePrefKey(String source, String pref) {
		return String.format("%s[%s].%s", KEY_SOURCE_PREFIX, source, pref);
	}
}
