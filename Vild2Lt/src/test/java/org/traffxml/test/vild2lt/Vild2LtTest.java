package org.traffxml.test.vild2lt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.locationtech.jts.geom.Point;
import org.opengis.feature.simple.SimpleFeature;

import net.iryndin.jdbf.core.DbfField;
import net.iryndin.jdbf.core.DbfMetadata;
import net.iryndin.jdbf.core.DbfRecord;
import net.iryndin.jdbf.reader.DbfReader;

public class Vild2LtTest {
	private static final int FLAG_DISSECT = 0x2;

	private static String getParam(String param, String[] args, int pos) {
		if(pos >= args.length) {
			System.out.println("-" + param + " needs an argument.");
			System.exit(1);
		}
		return args[pos];
	}

	private static void printUsage() {
		System.out.println("Arguments:");
		System.out.println("  -dissect            Examine VILD data and print results");
		System.out.println("  -dbf <path>         Process dBase file at <path>");
		System.out.println("  -shp <path>         Process shapefile at <path>");
		System.out.println("  -ltdb <path>        Use AlertC location database at the given path");
		System.out.println("  -ltef <path>        Write location tables in LTEF format to the given dir");
	}

	public static void main(String[] args) {
		String dbf = null;
		String shp = null;
		int flags = 0;

		if (args.length == 0) {
			printUsage();
			System.exit(1);
		}
		for (int i = 0; i < args.length; i++) {
			if ("-dissect".equals(args[i])) {
				flags |= FLAG_DISSECT;
			} else if ("-dbf".equals(args[i])) {
				dbf = getParam("dbf", args, ++i);
			} else if ("-shp".equals(args[i])) {
				shp = getParam("shp", args, ++i);
			} else if ("-ltdb".equals(args[i])) {
				System.out.println("Not supported yet!");
				System.exit(0);
			} else if ("-ltef".equals(args[i])) {
				System.out.println("Not supported yet!");
				System.exit(0);
			} else {
				System.out.println("Unknown argument: " + args[i]);
				System.out.println();
				printUsage();
				System.exit(0);
			}
		}

		switch (flags) {
		case 0:
			System.err.println("No operation specified.");
			System.exit(1);
			break;
		case FLAG_DISSECT:
			if ((dbf != null) && !dbf.isEmpty())
				dissectDbf(dbf);
			else
				System.err.println("No dBase file path specified; skipping.");
			if ((shp != null) && !shp.isEmpty())
				dissectShp(shp);
			else
				System.err.println("No SHP path specified; skipping.");
			break;
		default:
			System.err.println("Unsupported set of operations.");
			System.exit(1);
			break;
		}
	}

	/**
	 * Dumps the content of a dBase file.
	 * 
	 * @param dbf Path to the dBase file
	 */
	private static void dissectDbf(String dbf) {
		System.out.println("dBase: " + dbf);
		File file = new File(dbf);
		Charset charset = Charset.forName("ISO-8859-1");
		
		try {
			DbfRecord rec;
			DbfReader reader = new DbfReader(file);
			DbfMetadata meta = reader.getMetadata();
			System.out.println("  Metadata: " + meta);
			
			System.out.println("  Fields: ");
			for (DbfField field : meta.getFields()) {
				System.out.println("    " + field.getName() + ": " + field.getType() + "("
						+ field.getLength() + ", " + field.getNumberOfDecimalPlaces() + ")");
			}
			
			System.out.println("  Records: ");
			while ((rec = reader.read()) != null) {
				rec.setStringCharset(charset);
				System.out.println("    " + rec.toMap());
			}
		} catch (Throwable e) {
			e.printStackTrace(System.err);
		}
	}

	/**
	 * Dumps the content of a shape file.
	 * 
	 * The shape file must contain points with coordinates in WGS84.
	 * 
	 * @param shp Path to the shape file
	 */
	private static void dissectShp(String shp) {
		System.out.println("SHP: " + shp);
		File file = new File(shp);

		try {
			Map<String, String> connect = new HashMap<String, String>();
			connect.put("url", file.toURI().toString());

			DataStore dataStore = DataStoreFinder.getDataStore(connect);
			String[] typeNames = dataStore.getTypeNames();
			String typeName = typeNames[0];

			for (String name : typeNames)
				System.out.println("TypeName: " + name);

			System.out.println("Reading content " + typeName);

			SimpleFeatureSource featureSource = dataStore.getFeatureSource(typeName);
			SimpleFeatureCollection collection = featureSource.getFeatures();
			SimpleFeatureIterator iterator = collection.features();

			try {
				while (iterator.hasNext()) {
					SimpleFeature feature = iterator.next();
					System.out.println("Feature:");
					System.out.println("  Integer (LCID): " + feature.getAttribute(1));
					if (feature.getAttribute(0) instanceof Point) {
						Point point = (Point) feature.getAttribute(0);
						System.out.println("  X (lon): " + point.getX());
						System.out.println("  Y (lat): " + point.getY());
					} else {
						System.out.println("  Geometry: " + feature.getAttribute(0).getClass().getSimpleName());
						System.out.println("    " + feature.getAttribute(0).toString());
					}
				}
			} finally {
				iterator.close();
			}
		} catch (Throwable e) {
			e.printStackTrace(System.err);
		}
	}

}
