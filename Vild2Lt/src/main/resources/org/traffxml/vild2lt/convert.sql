insert into countries(cid, ecc, ccd, cname) values (39,'E3','8','Netherlands');
create table tmpnames(nid integer generated always as identity primary key, name varchar(100));
insert into tmpnames (name) select distinct first_name as name from tmpdbf where loc_nr != 0 and first_name is not null;
insert into tmpnames (name) select distinct secnd_name from tmpdbf where loc_nr != 0 and secnd_name is not null;
insert into tmpnames (name) select distinct roadname from tmpdbf where loc_nr != 0 and roadname is not null;
insert into names(cid, lid, nid, name, ncomment, officialname) select distinct 39 as cid, 1 as lid, nid, name, null as ncomment, null as officialname from tmpnames;
drop table tmpnames;
insert into POFFSETS select 39 as CID, (select max(tabcd) from locationdatasets where cid = 39 group by tabcd limit 1) as TABCD, loc_nr as LCD, case when neg_off = 0 then null else neg_off end as NEG_OFF_LCD, case when pos_off = 0 then null else pos_off end as POS_OFF_LCD from tmpdbf where loc_type like 'P%';
insert into SOFFSETS select 39 as CID, (select max(tabcd) from locationdatasets where cid = 39 group by tabcd limit 1) as TABCD, loc_nr as LCD, case when neg_off = 0 then null else neg_off end as NEG_OFF_LCD, case when pos_off = 0 then null else pos_off end as POS_OFF_LCD from tmpdbf where loc_type in ('L3.0', 'L4.0');
create table tmpareas(CID integer, TABCD integer, LCD integer primary key, CLASS varchar(1), TCD integer, STCD integer, NID integer, POL_LCD integer);
create index tmpareas_tcd on tmpareas(tcd);
insert into tmpareas(cid, tabcd, lcd, class, tcd, stcd, nid, pol_lcd) select 39 as cid, (select max(tabcd) from locationdatasets where cid = 39 group by tabcd limit 1) as tabcd, loc_nr as lcd, substr(loc_type, 1, 1) as class, substr(loc_type, 2, instr(loc_type, '.') - 2) as tcd, substr(loc_type, instr(loc_type, '.') + 1) as stcd, (select nid from names where names.name = tmpdbf.first_name limit 1) as nid, case when area_ref = 0 then null else area_ref end as pol_lcd from tmpdbf where loc_type like 'A%';
insert into ADMINISTRATIVEAREAS select * from tmpareas where tcd in (1, 2, 3, 7, 8, 9, 10, 11);
insert into OTHERAREAS select * from tmpareas where tcd not in (1, 2, 3, 7, 8, 9, 10, 11);
-- when pol_lcd refers to a non-admin area, replace it with the pol_lcd of its parent until we have an admin area
update otherareas set pol_lcd = (select pol_lcd from tmpareas where tmpareas.lcd = otherareas.pol_lcd) where pol_lcd in (select lcd from otherareas);
drop index tmpareas_tcd;
drop table tmpareas;
create table tmplines(CID integer, TABCD integer, LCD integer primary key, CLASS varchar(1), TCD integer, STCD integer, ROADNUMBER varchar(10), RNID integer, N1ID integer, N2ID integer, ROA_LCD integer, SEG_LCD integer, POL_LCD integer, PES_LEV integer, RDID integer);
create index tmplines_tcd on tmplines(tcd);
insert into tmplines select 39 as cid, (select max(tabcd) from locationdatasets where cid = 39 group by tabcd limit 1) as tabcd, loc_nr as lcd, substr(loc_type, 1, 1) as class, substr(loc_type, 2, instr(loc_type, '.') - 2) as tcd, substr(loc_type, instr(loc_type, '.') + 1) as stcd, substr(roadnumber, 1, 10) as roadnumber, (select nid from names where names.name = tmpdbf.roadname limit 1) as rnid, (select nid from names where names.name = tmpdbf.first_name limit 1) as n1id, (select nid from names where names.name = tmpdbf.secnd_name limit 1) as n2id, case when lin_ref = 0 then null else lin_ref end as roa_lcd, case when lin_ref = 0 then null else lin_ref end as seg_lcd, area_ref as pol_lcd, case when loc_type in ('L1.1', 'L6.1') then 1 else 2 end as pes_lev, null as rdid from tmpdbf where loc_type like 'L%';
-- when pol_lcd refers to a non-admin area, replace it with the pol_lcd of its parent until we have an admin area
update tmplines set pol_lcd = (select pol_lcd from otherareas where otherareas.lcd = tmplines.pol_lcd) where pol_lcd in (select lcd from otherareas);
insert into ROADS select CID, TABCD, LCD, CLASS, TCD, STCD, ROADNUMBER, RNID, N1ID, N2ID, POL_LCD, PES_LEV, RDID from tmplines where tcd not in (3, 4);
-- when seg_lcd refers to a road, set it to null
update tmplines set seg_lcd = null where seg_lcd in (select lcd from roads);
insert into SEGMENTS select CID, TABCD, LCD, CLASS, TCD, STCD, ROADNUMBER, RNID, N1ID, N2ID, ROA_LCD, SEG_LCD, POL_LCD, RDID from tmplines where tcd in (3, 4);
-- when roa_lcd refers to a segment, replace it with the roa_lcd of its parent until we have a road
update segments set roa_lcd = (select roa_lcd from tmplines where tmplines.lcd = segments.roa_lcd) where roa_lcd in (select lcd from segments);
drop index tmplines_tcd;
drop table tmplines;
insert into points select 39 as cid, (select max(tabcd) from locationdatasets where cid = 39 group by tabcd limit 1) as tabcd, loc_nr as lcd, substr(loc_type, 1, 1) as class, substr(loc_type, 2, instr(loc_type, '.') - 2) as tcd, substr(loc_type, instr(loc_type, '.') + 1) as stcd, case when junct_ref = 0 then null else junct_ref end as junctionnumber, (select nid from names where names.name = tmpdbf.roadname limit 1) as rnid, (select nid from names where names.name = tmpdbf.first_name limit 1) as n1id, (select nid from names where names.name = tmpdbf.secnd_name limit 1) as n2id, area_ref as pol_lcd, area_ref as oth_lcd, case when lin_ref = 0 then null else lin_ref end as seg_lcd, case when lin_ref = 0 then null else lin_ref end as roa_lcd, pos_in as inpos, neg_in as inneg, pos_out as outpos, neg_out as outneg, pres_pos as presentpos, pres_neg as presentneg, null as diversionpos, null as diversionneg, xcoord as xcoord, ycoord as ycoord, null as interruptsroad, 0 as urban, null as jnid from tmpdbf, tmpshp where lcd = loc_nr and loc_type like 'P%';
-- when oth_lcd refers to an admin area, set it to null
update points set oth_lcd = null where oth_lcd in (select lcd from administrativeareas);
-- when pol_lcd refers to a non-admin area, replace it with the pol_lcd of its parent until we have an admin area
update points set pol_lcd = (select pol_lcd from otherareas where otherareas.lcd = points.pol_lcd) where pol_lcd in (select lcd from otherareas);
-- when seg_lcd refers to a road, set it to null
update points set seg_lcd = null where seg_lcd in (select lcd from roads);
-- when roa_lcd refers to a segment, replace it with the roa_lcd of its parent until we have a road
update points set roa_lcd = (select roa_lcd from segments where segments.lcd = points.roa_lcd) where roa_lcd in (select lcd from segments);