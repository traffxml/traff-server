-- Permissions on tables (the user and role must be created beforehand)
grant select on Countries to lt_readonly;
grant select on LocationDataSets to lt_readonly;
grant select on Names to lt_readonly;
grant select on AdministrativeAreas to lt_readonly;
grant select on OtherAreas to lt_readonly;
grant select on Roads to lt_readonly;
grant select on Segments to lt_readonly;
grant select on Soffsets to lt_readonly;
grant select on Points to lt_readonly;
grant select on Poffsets to lt_readonly;
grant select on TabcdAliases to lt_readonly;
grant lt_readonly to LT;
