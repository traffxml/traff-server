package org.traffxml.vild2lt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.locationtech.jts.geom.Point;
import org.opengis.feature.simple.SimpleFeature;

import net.iryndin.jdbf.core.DbfField;
import net.iryndin.jdbf.core.DbfFieldTypeEnum;
import net.iryndin.jdbf.core.DbfMetadata;
import net.iryndin.jdbf.core.DbfRecord;
import net.iryndin.jdbf.reader.DbfReader;

public class Vild2Lt {
	private static final String DB_ROLE_READONLY = "lt_readonly";
	private static final String DB_USER_LT = "LT";
	static final String MEM_DB_URL = "jdbc:hsqldb:mem:.";
	static final String[] TABLES = {
			"COUNTRIES",
			"LOCATIONDATASETS",
			"NAMES",
			"ADMINISTRATIVEAREAS",
			"OTHERAREAS",
			"ROADS",
			"SEGMENTS",
			"SOFFSETS",
			"POINTS",
			"POFFSETS",
	};
	
	static Connection tempDb = null;

	private static String getParam(String param, String[] args, int pos) {
		if(pos >= args.length) {
			System.out.println("-" + param + " needs an argument.");
			System.exit(1);
		}
		return args[pos];
	}

	private static void printUsage() {
		System.out.println("Arguments:");
		System.out.println("  -dbf <path>         Process dBase file at <path>");
		System.out.println("  -shp <path>         Process shapefile at <path>");
		System.out.println("  -ltdb <path>        Use AlertC location database at the given path");
		System.out.println("  -user <user>        Authenticate as <user> (only with -ltdb)");
		System.out.println("  -pwd <pwd>          Authenticate with password <pwd> (only with -user)");
		System.out.println("  -ltef <path>        Write location tables in LTEF format to <path>");
	}

	public static void main(String[] args) {
		String dbf = null;
		String shp = null;
		String ltdb = null;
		String ltef = null;
		String user = null;
		String pwd = null;

		if (args.length == 0) {
			printUsage();
			System.exit(1);
		}
		for (int i = 0; i < args.length; i++) {
			if ("-dbf".equals(args[i])) {
				dbf = getParam("dbf", args, ++i);
			} else if ("-shp".equals(args[i])) {
				shp = getParam("shp", args, ++i);
			} else if ("-ltdb".equals(args[i])) {
				ltdb = getParam("ltdb", args, ++i);
				if ((ltdb != null) && !ltdb.isEmpty())
					ltdb = String.format("jdbc:hsqldb:file:%s", ltdb);
			} else if ("-user".equals(args[i])) {
				user = getParam("user", args, ++i);
			} else if ("-pwd".equals(args[i])) {
				pwd = getParam("pwd", args, ++i);
			} else if ("-ltef".equals(args[i])) {
				ltef = getParam("ltef", args, ++i);
			} else {
				System.out.println("Unknown argument: " + args[i]);
				System.out.println();
				printUsage();
				System.exit(0);
			}
		}
		if ((dbf == null) || dbf.isEmpty() || (shp == null) || shp.isEmpty()) {
			System.out.println("Both -dbf and -shp must be specified.");
			System.exit(1);
		}
		if (((ltdb == null) || ltdb.isEmpty()) && ((ltef == null) || ltef.isEmpty())) {
			System.out.println("Either -ltdb or -ltef must be specified.");
			System.exit(1);
		}
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver" );
		} catch (Exception e) {
			System.err.println("ERROR: failed to load HSQLDB JDBC driver.");
			e.printStackTrace();
			System.exit(1);
		}
		try {
			tempDb = DriverManager.getConnection(MEM_DB_URL);
			tempDb.setAutoCommit(false);
		} catch (SQLException e) {
			System.err.println("ERROR: failed to open in-memory DB.");
			e.printStackTrace(System.err);
			System.exit(1);
		}
		System.out.println("Creating tables");
		runDbScript(tempDb, "createLtTables.sql");
		runDbScript(tempDb, "createImportTables.sql");
		importDbf(tempDb, dbf);
		importShp(tempDb, shp);
		System.out.println("Converting data");
		runDbScript(tempDb, "convert.sql");

		// write LTEF files if requested
		if ((ltef != null) && !ltef.isEmpty()) {
			File ltefDir = new File(ltef);
			if (!ltefDir.exists())
				try {
					ltefDir.mkdirs();
				} catch (Throwable e) {
					System.err.println("Could not create LTEF output directory.");
					e.printStackTrace();
					System.exit(1);
				}
			else if (!ltefDir.isDirectory()) {
				System.err.println("-ltef must specify a directory");
				System.exit(1);
			}
			for (String table : TABLES)
				exportTable(tempDb, ltefDir, table);
		}

		// populate LTDB if requested
		if ((ltdb != null) && !ltdb.isEmpty()) {
			Connection ltdbConnection = null;
			try {
				System.out.println("Importing data into LTDB at " + ltdb);
				if ((user != null) && !user.isEmpty())
					ltdbConnection = DriverManager.getConnection(ltdb, user, pwd);
				else
					ltdbConnection = DriverManager.getConnection(ltdb);
				System.out.println("Creating users and roles in destination DB");
				createDbUser(ltdbConnection, DB_USER_LT, "");
				createDbRole(ltdbConnection, DB_ROLE_READONLY);
				System.out.println("Creating tables in destination DB");
				runDbScript(ltdbConnection, "createLtTables.sql");
				System.out.println("Setting permissions in destination DB");
				runDbScript(ltdbConnection, "setLtPerms.sql");
				purgeLt(tempDb, ltdbConnection);
				for (String table : TABLES)
					mergeTable(tempDb, ltdbConnection, table);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			} finally {
				if (ltdbConnection != null)
					try {
						ltdbConnection.close();
					} catch (SQLException e) { /* NOP */ }
			}
		}
		System.out.println("Done.");
	}

	/**
	 * Exports data from a table into a CSV file.
	 * 
	 * The export file will be named like the table, with a {@code .DAT} extension. An exception is the
	 * {@code ADMINISTRATIVEAREAS} table, which is exported to {@code ADMINISTRATIVEAREA.DAT}.
	 * 
	 * @param connection The connection to the source database
	 * @param ltefDir The directory in which to create the export file
	 * @param table The table name to merge
	 */
	private static void exportTable(Connection connection, File ltefDir, String table) {
		File dest;
		if ("ADMINISTRATIVEAREAS".equals(table))
			dest = new File(ltefDir, "ADMINISTRATIVEAREA.DAT");
		else
			dest = new File(ltefDir, table + ".DAT");
		System.out.println("Writing " + dest.getPath());
		try {
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM " + table + ";");
			ResultSet rset = stmt.executeQuery();
			ResultSetMetaData meta = rset.getMetaData();
			FileWriter writer = new FileWriter(dest);
			try {
				// column names
				for (int i = 1; i <= meta.getColumnCount(); i++) {
					if (i > 1)
						writer.write(";");
					writer.write(meta.getColumnLabel(i));
				}
				writer.write("\n");

				// column values
				while (rset.next()) {
					for (int i = 1; i <= meta.getColumnCount(); i++) {
						if (i > 1)
							writer.write(";");
						Object object = rset.getObject(i);
						if (object != null)
							writer.write(object.toString());
					}
					writer.write("\n");
				}
			} finally {
				writer.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Imports the content of a dBase file as a temporary table.
	 * 
	 * @param dbf Path to the dBase file
	 */
	private static void importDbf(Connection connection, String dbf) {
		System.out.println("Importing data from DBF: " + dbf);
		File file = new File(dbf);
		Charset charset = Charset.forName("ISO-8859-1");
		
		DbfReader reader = null;
		try {
			reader = new DbfReader(file);
			// create temporary table
			DbfMetadata meta = reader.getMetadata();
			StringBuilder stmtBuilder = new StringBuilder("create cached table tmpdbf(");
			List<String> fields = new ArrayList<String>();
			StringBuilder fieldsBuilder = new StringBuilder();
			StringBuilder valuesBuilder = new StringBuilder();
			String separator = null;
			for (DbfField field : meta.getFields()) {
				stmtBuilder.append(field.getName());
				DbfFieldTypeEnum type = field.getType();
				switch(type) {
				case Character:
					stmtBuilder.append(" varchar(").append(field.getLength()).append(")");
					break;
				case Numeric:
					stmtBuilder.append(" integer");
					break;
				default:
					throw new IllegalStateException("DBF uses unsupported type " + type);
				}
				stmtBuilder.append(", ");
				if (separator == null)
					separator = ", ";
				else {
					fieldsBuilder.append(separator);
					valuesBuilder.append(separator);
				}
				fields.add(field.getName());
				// TODO can we format the list and drop fieldBuilder?
				fieldsBuilder.append(field.getName());
				valuesBuilder.append("?");
			}
			stmtBuilder.append("primary key(LOC_NR));");
			PreparedStatement stmt = connection.prepareStatement(stmtBuilder.toString());
			stmt.execute();
			connection.commit();
			stmt.close();
			
			// import data into temporary table
			DbfRecord rec;
			stmt = connection.prepareStatement("insert into tmpdbf(" + fieldsBuilder.toString() + ") values ("
					+ valuesBuilder.toString() + ");");
			while ((rec = reader.read()) != null) {
				// LOC_NR = 0 is the header record; handle specially
				String locNr = rec.getString("LOC_NR");
				if ("0".equals(locNr)) {
					PreparedStatement stmt0 = connection.prepareStatement("insert into locationdatasets(cid, tabcd, dcomment, version, versiondescription) values (?, ?, ?, ?, ?);");
					// cid = 39 (Netherlands)
					stmt0.setInt(1, 39);

					// Generate tabcd from version (two-digit minor, 5.2 = 5.20)
					String tabcd;
					String version = rec.getString("FIRST_NAME");
					Pattern pattern = Pattern.compile(".*?([0-9]+)\\.([0-9]+).*");
					Matcher matcher = pattern.matcher(version);
					if (!matcher.matches() || (matcher.groupCount() == 0))
						tabcd = "0";
					else if (matcher.groupCount() == 1)
						tabcd = matcher.group(1) + "00";
					else
						tabcd = matcher.group(1) + (matcher.group(2) + "00").substring(0, 2);
					System.out.println("The TABCD for the processed location table is: " + tabcd);
					stmt0.setInt(2, Integer.valueOf(tabcd));

					stmt0.setNull(3, java.sql.Types.VARCHAR);
					stmt0.setString(4, rec.getString("FIRST_NAME"));
					stmt0.setString(5, rec.getString("SECND_NAME"));
					stmt0.execute();
					stmt0.close();
					continue;
				}

				rec.setStringCharset(charset);
				stmt.clearParameters();
				int i = 1;
				for (String field : fields) {
					if (rec.getField(field).getType() == DbfFieldTypeEnum.Character)
						stmt.setString(i, rec.getString(field));
					else if (rec.getField(field).getType() == DbfFieldTypeEnum.Numeric)
						stmt.setInt(i, Integer.valueOf(rec.getString(field)));
					else
						throw new IllegalStateException("DBF uses unsupported type " + rec.getField(field).getType());
					i++;
				}
				stmt.addBatch();
			}
			stmt.executeBatch();
			connection.commit();
			stmt.close();
		} catch (Throwable e) {
			e.printStackTrace(System.err);
		} finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException e) {
				// NOP
			}
		}
	}

	/**
	 * Imports the content of a shape file as a temporary table.
	 * 
	 * The shape file must contain points with coordinates in WGS84.
	 * 
	 * @param connection The JDBC connection
	 * @param shp Path to the shape file
	 */
	private static void importShp(Connection connection, String shp) {
		System.out.println("Importing points from SHP: " + shp);
		File file = new File(shp);

		try {
			Map<String, String> connect = new HashMap<String, String>();
			connect.put("url", file.toURI().toString());

			DataStore dataStore = DataStoreFinder.getDataStore(connect);
			String[] typeNames = dataStore.getTypeNames();

			SimpleFeatureSource featureSource = dataStore.getFeatureSource(typeNames[0]);
			SimpleFeatureCollection collection = featureSource.getFeatures();
			SimpleFeatureIterator iterator = collection.features();

			PreparedStatement stmt = null;
			try {
				stmt = connection.prepareStatement("insert into tmpshp(lcd, xcoord, ycoord) values (?, ?, ?);");
				while (iterator.hasNext()) {
					SimpleFeature feature = iterator.next();
					if (feature.getAttribute(0) instanceof Point) {
						stmt.clearParameters();
						stmt.setInt(1, (Integer) feature.getAttribute(1));
						Point point = (Point) feature.getAttribute(0);
						stmt.setDouble(2, point.getX());
						stmt.setDouble(3, point.getY());
						stmt.addBatch();
					} else {
						System.err.println("Point " + feature.getAttribute(1) + "is not a point: " + feature.getAttribute(0).getClass().getSimpleName());
						System.out.println("  " + feature.getAttribute(0).toString());
					}
				}
				stmt.executeBatch();
			} finally {
				if (stmt != null)
					stmt.close();
				iterator.close();
				connection.commit();
			}
		} catch (Throwable e) {
			e.printStackTrace(System.err);
		}
	}

	/**
	 * Merges data from a table from the source database into a table of the destination database.
	 * 
	 * Both tables must have the same name and contain the same columns (i.e. with the same name and type, in
	 * the same order), and records in the source table must satisfy all constraints on the destination table
	 * (which is guaranteed only if the destination table has the same, or less stringent, constrainta as the
	 * source table).
	 * 
	 * @param source The connection to the source database
	 * @param dest The connection to the destination database
	 * @param table The table name to merge
	 * @throws SQLException
	 */
	private static void mergeTable(Connection source, Connection dest, String table)
			throws SQLException {
		System.out.println("Merging " + table);
		PreparedStatement stmtQ = null;
		ResultSet rset = null;
		PreparedStatement stmtI = null;
		try {
			stmtQ = source.prepareStatement("SELECT * FROM " + table + ";");
			rset = stmtQ.executeQuery();
			ResultSetMetaData meta = rset.getMetaData();
			// prepare the insert query
			StringBuilder stmtIBuilder = new StringBuilder("INSERT INTO ");
			stmtIBuilder.append(table).append(" (");
			for (int i = 1; i <= meta.getColumnCount(); i++) {
				if (i > 1)
					stmtIBuilder.append(", ");
				stmtIBuilder.append(meta.getColumnLabel(i));
			}
			stmtIBuilder.append(") VALUES (");
			for (int i = 1; i <= meta.getColumnCount(); i++)
				stmtIBuilder.append(i == 1 ? "?" : ", ?");
			stmtIBuilder.append(");");
			stmtI = dest.prepareStatement(stmtIBuilder.toString());

			// insert the records
			while (rset.next()) {
				stmtI.clearParameters();
				for (int i = 1; i <= meta.getColumnCount(); i++)
					stmtI.setObject(i, rset.getObject(i));
				stmtI.addBatch();
			}
			stmtI.executeBatch();
			dest.commit();
		} finally {
			if (stmtI != null)
				try {
					stmtI.close();
				} catch (SQLException e) { /* NOP */ }
			if (rset != null)
				try {
					rset.close();
				} catch (SQLException e) { /* NOP */ }
			if (stmtQ != null)
				try {
					stmtQ.close();
				} catch (SQLException e) { /* NOP */ }
		}
	}

	/**
	 * Deletes location table data from the destination database if it is present in the source.
	 * 
	 * This is intended to be used before calling {@link #mergeTable(Connection, Connection, String)}, to
	 * ensure the location table to be imported is not already present in the destination database (which
	 * would cause the update to fail).
	 * 
	 * @param source The connection to the source database
	 * @param dest The connection to the destination database
	 * @throws SQLException
	 */
	private static void purgeLt(Connection source, Connection dest) throws SQLException {
		System.out.println("Purging stale location data");
		PreparedStatement stmtQ = null;
		ResultSet rset = null;
		PreparedStatement stmtI = null;
		try {
			stmtQ = source.prepareStatement("SELECT CID, TABCD FROM LOCATIONDATASETS;");
			rset = stmtQ.executeQuery();
			stmtI = dest.prepareStatement("DELETE FROM LOCATIONDATASETS WHERE CID = ? AND TABCD = ?;");
			while (rset.next()) {
				stmtI.clearParameters();
				for (int i = 1; i <= 2; i++)
					stmtI.setObject(i, rset.getObject(i));
				stmtI.addBatch();
			}
			stmtI.executeBatch();
			dest.commit();
		} finally {
			if (stmtI != null)
				try {
					stmtI.close();
				} catch (SQLException e) { /* NOP */ }
			if (rset != null)
				try {
					rset.close();
				} catch (SQLException e) { /* NOP */ }
			if (stmtQ != null)
				try {
					stmtQ.close();
				} catch (SQLException e) { /* NOP */ }
		}
	}

	/**
	 * Creates a role in the database, unless it exists already.
	 * 
	 * IMPORTANT: When passing external input (e.g. from the user or a remote system) as {@code role},
	 * it must be sanitized, as there is no protection against SQL injection. If the value is obtained
	 * from a trustworthy source, this is not a concern.
	 * 
	 * Adding an already existing role is a no-op.
	 * 
	 * @param connection The connection to the database
	 * @param role The new role
	 */
	static void createDbRole(Connection connection, String role) {
		PreparedStatement stmtQ = null;
		ResultSet rset = null;
		PreparedStatement stmtI = null;
		try {
			stmtQ = connection.prepareStatement("SELECT ROLE_NAME FROM INFORMATION_SCHEMA.APPLICABLE_ROLES WHERE UCASE(ROLE_NAME) = UCASE(?);");
			stmtQ.setString(1, role);
			rset = stmtQ.executeQuery();
			if (!rset.next()) {
				System.out.println("Role " + role + " does not exist yet, creating it");
				stmtI = connection.prepareStatement("CREATE ROLE " + role + ";");
				stmtI.execute();
				connection.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace(System.err);
			return;
		} finally {
			if (stmtI != null)
				try {
					stmtI.close();
				} catch (SQLException e) { /* NOP */ }
			if (rset != null)
				try {
					rset.close();
				} catch (SQLException e) { /* NOP */ }
			if (stmtQ != null)
				try {
					stmtQ.close();
				} catch (SQLException e) { /* NOP */ }
		}
	}

	/**
	 * Creates a user in the database, unless it exists already.
	 * 
	 * IMPORTANT: When passing external input (e.g. from the user or a remote system) as {@code user} or
	 * {@code password}, they must be sanitized, as there is no protection against SQL injection. If the
	 * values are obtained from a trustworthy source, this is not a concern.
	 * 
	 * Adding an already existing user is a no-op.
	 * 
	 * @param connection The connection to the database
	 * @param user The new user
	 * @param password The password for the new user
	 */
	static void createDbUser(Connection connection, String user, String password) {
		PreparedStatement stmtQ = null;
		ResultSet rset = null;
		PreparedStatement stmtI = null;
		try {
			stmtQ = connection.prepareStatement("SELECT USER_NAME FROM INFORMATION_SCHEMA.SYSTEM_USERS WHERE UCASE(USER_NAME) = UCASE(?);");
			stmtQ.setString(1, user);
			rset = stmtQ.executeQuery();
			if (!rset.next()) {
				System.out.println("User " + user + " does not exist yet, creating it");
				stmtI = connection.prepareStatement("CREATE USER " + user + " PASSWORD '" + password + "';");
				stmtI.execute();
				connection.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace(System.err);
			return;
		} finally {
			if (stmtI != null)
				try {
					stmtI.close();
				} catch (SQLException e) { /* NOP */ }
			if (rset != null)
				try {
					rset.close();
				} catch (SQLException e) { /* NOP */ }
			if (stmtQ != null)
				try {
					stmtQ.close();
				} catch (SQLException e) { /* NOP */ }
		}
	}

	/**
	 * Runs a script from a resource file on a database connection.
	 * 
	 * @param connection The JDBC connection
	 * @param script The name of a resource file with the script
	 */
	static void runDbScript(Connection connection, String script) {
		InputStream stream = Vild2Lt.class.getResourceAsStream(script);
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				// skip comment and empty lines
				if (line.trim().startsWith("--") || line.trim().isEmpty())
					continue;
				try {
					PreparedStatement stmt = connection.prepareStatement(line);
					stmt.execute();
					connection.commit();
					stmt.close();
				} catch (SQLException e) {
					System.err.println("SQLException encountered while executing statement\n  " + line);
					e.printStackTrace(System.err);
					return;
				}	
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
